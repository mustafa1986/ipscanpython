import ipaddress

def checkParseIp(inputIP):
    iplist = []
    ipchecker = 0
    if "-" in inputIP:
        ipstart, ipend = inputIP.split("-")
        try:
            ipaddress.ip_address(ipstart)
            ipchecker = 1
        except:
            print("Daxil etdiyiniz araliq {} duzgun deyil".format(ipstart))

        _ipstart = ipstart.split(".")
        a = "."
        if int(ipend) > 0 and int(ipend) <= 255 and ipchecker == 1:
            for i in range(int(_ipstart[-1]), int(ipend)):
                iplist.append(_ipstart[0] + a + _ipstart[1] + a + _ipstart[2] + a + str(i))
        else:
            print("Araliq {} duzgun deyil ".format(ipend))

    if "/" in inputIP:
        try:
            inputIP = ipaddress.ip_network(inputIP)
            for x in inputIP.hosts():
                iplist.append(x)
        except ValueError:
            print("Daxil etdiyiniz {} subnet duzgun deyil!".format(inputIP))

    elif "/" not in inputIP and "-" not in inputIP:
        try:
            ipaddress.ip_address(inputIP)
            iplist.append(inputIP)
        except ValueError:
            print("IP address duzgun deyil!!!")
    return iplist
