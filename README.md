# Installing

git clone https://gitlab.com/mustafa1986/ipscanpython.git

cd ipscanpython

python3 MainMenu.py --help 

# Example

python3 MainMenu.py -i 192.168.0.5 -p 445,21,25,443 (Scan IP and port )

python3 MainMenu.py -i 192.168.0.5-100 -p21-100 (Scan IP and port )

python3 MainMenu.py -s 192.168.0.0/24 -p 80,443  (Scan subnet and port)
