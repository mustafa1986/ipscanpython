def checkParsePort(inputPort):
    portlist = []
    if "-" in inputPort:
        startPort, endPort = inputPort.split("-")
        for i in range(int(startPort), int(endPort) + 1):
            portlist.append(i)
    elif "," in inputPort:
        inputPort = inputPort.split(",")
        for i in inputPort:
            portlist.append(i)
    elif " " in inputPort:
        inputPort = inputPort.split(" ")
        for i in inputPort:
            portlist.append(i)
    elif "-" not in inputPort or "," not in inputPort or " " not in inputPort:
        portlist.append(inputPort)
    else:
        print("Port duzgun deyil")
    return portlist
