from socket import *
import argparse
import sys
import CheckParseIP
import CheckParsePort
from datetime import datetime


def menu():
    parser = argparse.ArgumentParser(description="port scanner")
    parser.add_argument('-i', help='IP. Example:python3 portscan.py -i 192.168.0.0/24 or 192.168.0.5-100 or 192.168.0.5', dest="ip", type=str)
    parser.add_argument('-p', help='Port araligi  daxil edin. Example: python3 portscan.py -p 21-135 or 21,22,135,80', dest="port", type=str,required=True)
    if len(sys.argv) < 3:
        parser.print_help()
        sys.exit(0)
    args = parser.parse_args()
    return args


argument = menu()
ip = argument.ip
port = argument.port
iplist = CheckParseIP.checkParseIp(ip)
portlist = CheckParsePort.checkParsePort(port)


def scannerIP(ip, port):
    t1 = datetime.now()
    print("Scan basladi")
    for _ip in ip:
        for _port in port:
            location = (_ip, int(_port))
            conn = socket(AF_INET, SOCK_STREAM)
            resultconn = conn.connect_ex(location)
            if resultconn == 0:
                print("Port {} is open in  {}".format(_port, _ip))
            if resultconn != 0:
                print("Port {} is close in  {}".format(_port, _ip))
    t2 = datetime.now()
    total = t2 - t1
    print("Scan bitdi:", total)


scannerIP(iplist, portlist)
